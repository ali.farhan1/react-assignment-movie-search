import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import defaultProps from "default-props";
import propTypes from "prop-types";
import Grid from "@mui/material/Grid";
import Pagination from "@mui/material/Pagination";
import MovieCard from "../MovieCard/MovieCard";
import Progress from "../Progress/Progress";
import MovieModal from "../MovieModel/MovieModal";
import { moviesLoadNew } from "../../store/actions";
import "./CardsGallary.css";

const CardsGallary = ({ spacing }) => {
    const dispatch = useDispatch();
    const [page, setPage] = useState(1);
    const [isModalOpen, setModalOpen] = useState(false);
    const [currentMovie, setCurrentMovie] = useState({});
    const movies = useSelector((state) => state.movies.movies);
    const mode = useSelector((state) => state.mode);
    const isLoading = useSelector((state) => state.movies.loading);
    const totalPages = useSelector((state) => state.movies.totalPages);
    const url = useSelector((state) => state.url.url);

    const handleModal = (movie) => {
        setCurrentMovie(movie);
        setModalOpen(true);
    };

    const handleChange = (event, value) => {
        event.preventDefault();
        setPage(value);
        dispatch(moviesLoadNew(`${url}&page=${value}`));
    };

    if (movies.length < 1) {
        dispatch(moviesLoadNew(`${url}&page=1`));
    }

    return isLoading ? (
        <Progress />
    ) : (
        <Grid
            sx={{ flexGrow: 1, p: 2 }}
            container
            spacing={2}
            className="cards--container"
        >
            <MovieModal
                isOpen={isModalOpen}
                setOpen={setModalOpen}
                movie={currentMovie}
            />
            <Grid item xs={12} sx={{ marginBottom: 2 }}>
                <Grid container justifyContent="center" spacing={spacing}>
                    {movies.map((movie) => (
                        <Grid key={movie.id} item>
                            <MovieCard
                                movie={movie}
                                mode={mode}
                                handleModal={handleModal}
                            ></MovieCard>
                        </Grid>
                    ))}
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Grid container justifyContent="center" spacing={spacing}>
                    <Pagination
                        count={totalPages}
                        page={page}
                        onChange={handleChange}
                    />
                </Grid>
            </Grid>
        </Grid>
    );
};

CardsGallary.propTypes = {
    movies: propTypes.array,
    spacing: propTypes.number,
    totalPages: propTypes.number,
    url: propTypes.string,
    loadNew: propTypes.func,
    loading: propTypes.bool,
    setLoading: propTypes.func,
};

CardsGallary.defaultProps = defaultProps;

CardsGallary.defaultProps = {
    movies: [],
    mode: "light",
    spacing: 2,
    totalPages: 10,
    url: "",
    loadNew: () => {},
    loading: false,
    setLoading: () => {},
};

export default CardsGallary;
