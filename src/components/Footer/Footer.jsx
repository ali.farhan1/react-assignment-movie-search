// Import Statements

import React from "react";
import { useSelector } from "react-redux";
import propTypes from "prop-types";
import defaultProps from "default-props";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import "./Footer.css";

// Definition Statements

const Footer = () => {
    const mode = useSelector((state) => state.mode);
    return (
        <div className={`footer ${mode}`}>
            <Typography variant="body2">
                {"Copyright © "}
                <Link
                    color="inherit"
                    href="https://www.linkedin.com/in/farhan-kiyani/"
                    target="_blank"
                >
                    Farhan Kiyani
                </Link>{" "}
                {new Date().getFullYear()}
                {"."}
            </Typography>
        </div>
    );
};

Footer.propTypes = {
    mode: propTypes.string,
};

Footer.defaultProps = defaultProps;

Footer.defaultProps = {
    mode: "light",
};

export default Footer;
