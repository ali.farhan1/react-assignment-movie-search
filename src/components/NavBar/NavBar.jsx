// Material UI App Bar with Search Bar and logo

import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import propTypes from "prop-types";
import defaultProps from "default-props";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import SearchBar from "../SearchBar/SearchBar";
import { changeMode } from "../../store/actions";
import logo from "../../assets/logo/logo192.png";
import "./Navbar.css";

const NavBar = ({ logoSrc, placeholder }) => {
    const dispatch = useDispatch();

    const mode = useSelector((state) => state.mode);
    const [lightMode, setLightMode] = useState(false);

    const handleClick = () => {
        setLightMode(!lightMode);
    };

    useEffect(() => {
        lightMode
            ? dispatch(changeMode("light"))
            : dispatch(changeMode("dark"));
    }, [lightMode, dispatch]);

    return (
        <div>
            <AppBar position="static">
                <Toolbar className={`NavBar ${mode}`}>
                    <img
                        src={logoSrc}
                        alt="logo"
                        className={`logo-img`}
                        onClick={handleClick}
                    />
                    <SearchBar placeholder={placeholder} width={800} />
                </Toolbar>
            </AppBar>
        </div>
    );
};

NavBar.propTypes = {
    logoSrc: propTypes.string,
    placeholder: propTypes.string,
};

NavBar.defaultProps = defaultProps;

NavBar.defaultProps = {
    logoSrc: logo,
    placeholder: "Search Movies",
};

export default NavBar;
