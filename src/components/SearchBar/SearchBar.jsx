import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import propTypes from "prop-types";
import defaultProps from "default-props";
import SearchIcon from "@mui/icons-material/Search";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import { moviesLoadNew, urlSet } from "../../store/actions";
import SearchButton, { VARIANTS } from "../SearchButton/SearchButton";
import "./SearchBar.css";

const SearchBar = ({ placeholder, width }) => {
    const dispatch = useDispatch();
    const [SearchBarClass, setSearchBarClass] = useState("");
    const [searchValue, setSearchValue] = useState("");
    const mode = useSelector((state) => state.mode);
    const url = useSelector((state) => state.url.url);
    useEffect(() => {
        mode === "light"
            ? setSearchBarClass("search-bar-light")
            : setSearchBarClass("search-bar-dark");
    }, [mode]);

    useEffect(() => {
        if (searchValue !== "") {
            dispatch(
                urlSet(
                    `/search/movie?api_key=${process.env.REACT_APP_API_KEY}&language=en-US&query=${searchValue}`
                )
            );
        } else {
            dispatch(
                urlSet(
                    `/movie/top_rated?api_key=${process.env.REACT_APP_API_KEY}&language=en-US`
                )
            );
        }
    }, [searchValue, dispatch]);

    const clickHandler = (e) => {
        e.preventDefault();
        dispatch(moviesLoadNew(url));
    };

    return (
        <Paper
            component="form"
            sx={{
                p: "2px 4px",
                display: "flex",
                alignItems: "center",
                width: { width },
            }}
            className={SearchBarClass}
        >
            <InputBase
                sx={{ ml: 1, flex: 1 }}
                placeholder={placeholder}
                inputProps={{ "aria-label": "search movies" }}
                value={searchValue}
                onChange={(e) => setSearchValue(e.target.value)}
                className={SearchBarClass}
            />
            <SearchButton
                variant={VARIANTS.contained}
                clickHandler={clickHandler}
            >
                <SearchIcon />
            </SearchButton>
        </Paper>
    );
};

SearchBar.propTypes = {
    placeholder: propTypes.string,
    width: propTypes.number,
};

SearchBar.defaultProps = defaultProps;

SearchBar.defaultProps = {
    placeholder: "Search Movies",
    width: "100%",
    error: "",
};

export default SearchBar;
