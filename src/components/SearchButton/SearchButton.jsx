import React from "react";
import propTypes from "prop-types";
import defaultProps from "default-props";
import { useSelector } from "react-redux";
import Button from "@mui/material/Button";
import SearchIcon from "@mui/icons-material/Search";
import "./SearchButton.css";

export const VARIANTS = {
    outline: "outline",
    contained: "contained",
};

const SearchButton = ({ variant, children, icon, clickHandler, type }) => {
    const mode = useSelector((state) => state.mode);
    return (
        <Button
            variant={variant}
            className={` ${mode} search-button`}
            endIcon={icon ? <SearchIcon /> : null}
            onClick={clickHandler}
            type={type}
        >
            {children}
        </Button>
    );
};

SearchButton.propTypes = {
    variant: propTypes.oneOf(Object.values(VARIANTS)),
    children: propTypes.node,
    icon: propTypes.bool,
    clickHandler: propTypes.func,
    type: propTypes.string,
};

SearchButton.defaultProps = defaultProps;

SearchButton.defaultProps = {
    variant: VARIANTS.contained,
    children: "Search",
    icon: false,
    clickHandler: () => "Search Button clicked",
    type: "submit",
};

export default SearchButton;
