import { render, screen } from "@testing-library/react";
import SearchButton from "./SearchButton";
import store from "../../store/store";
import { Provider } from "react-redux";

test("renders learn react link", () => {
    render(
        <Provider store={store}>
            <SearchButton />
        </Provider>
    );
});
