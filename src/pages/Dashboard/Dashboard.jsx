import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import NavBar from "../../components/NavBar/NavBar";
import CardsGallary from "../../components/CardsGallary/CardsGallary";
import Footer from "../../components/Footer/Footer";
import "./Dashboard.css";

const Dashboard = () => {
    return (
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="xl" className="dashboard">
                <NavBar placeholder="Search for a movie" />
                <CardsGallary />
                <Footer />
            </Container>
        </React.Fragment>
    );
};

export default Dashboard;
